var nl = require('os').EOL,
	zlib = require('zlib'),
	sentiment = require('./sentiment'),
	Delayed = require('./delayed'),
	filters = [],
	D = function(x) { return new Date(x); },
	T = function(x) { return ~~(D(x).getTime() / 1000)};

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add = function(header, func)
{
	func.header = header;
	this.push(func);
}

//==============================================================================

//                                  General

filters.add('Post ID', function (tag) { return +tag.id; });

//------------------------------------------------------------------------------

//                                 Sentiment

filters.add('Sentiment', function (tag)
{
	if (!sentiment)
		return 'NA';
	
	var value = new Delayed;
	sentiment(tag.title + nl + tag.body).on('done', function (score)
	{
		value.done(score);
	});
	return value;
});

//------------------------------------------------------------------------------

//                                  Rating

filters.add('Score', function (tag) { return +tag.score; });

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add('Favorite count', function (tag)
	{ return +(tag.favoritecount || 0); });

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add('Accepted answer score', function (tag)
{
	if (!(+tag.id in this.AcceptedAnswerRating))
		return 'NA';

	return this.AcceptedAnswerRating[+tag.id];
});

//------------------------------------------------------------------------------

//                                Popularity

filters.add('View count', function (tag) { return +tag.viewcount; });

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add('Answer count', function (tag) { return +tag.answercount; });

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add('Comment count', function (tag) { return +tag.commentcount; });

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add('Total comment count', function (tag)
{
	return (this.AnswerCommentsCount[+tag.id] || 0) + +tag.commentcount;
});

//------------------------------------------------------------------------------

//                                  Urgency

filters.add('Response time', function (tag)
{
	if (!(+tag.id in this.ResponseTime))
		return 'NA';

	return this.ResponseTime[+tag.id] - T(tag.creationdate);
});

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add('Average response time', function (tag)
{
	if (!(+tag.id in this.AverageTime))
		return 'NA';

	return ~~(this.AverageTime[+tag.id][0] - T(tag.creationdate));
});

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

filters.add('Close time', function (tag)
{
	if (!tag.closeddate)
		return 'NA';

	return T(tag.closeddate) - T(tag.creationdate);
});

//------------------------------------------------------------------------------

//                                 Complexity

filters.add('Body size', function (tag)
{
	return tag.body.replace(/(\r\n|\n)/g, '').length;
});

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

// Naive Kolmogorov complexity approximation
filters.add('Body complexity', function (tag)
{
	var value = new Delayed;
	zlib.deflate(tag.body, function(err, data)
	{
		if (err)
			value.done('NA');
		else
			value.done(data.length);
	});
	return value;
})

//==============================================================================

module.exports = filters;

//------------------------------------------------------------------------------