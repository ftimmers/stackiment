var fs = require('fs'),
	events = require('events'),
	sax = require('sax'),
	BUFFER_SIZE = 16384;

//------------------------------------------------------------------------------

function readfile(filename)
{
	var fp = fs.openSync(filename, 'r'),
		buffer = new Buffer(BUFFER_SIZE),
		event = new events.EventEmitter();

	(function parse()
	{
		fs.read(fp, buffer, 0, BUFFER_SIZE, null, function(err, len, buf)
		{
			event.emit('read', buf.slice(0, len));
			if (!err && len)
				parse();
			else if (!len)
				event.emit('done');
			else
				event.emit('error', err);
		});
	})();

	return event;
}

//------------------------------------------------------------------------------

function parsefile(filename)
{
	var parser = sax.parser(false, { lowercase: true }),
		event = new events.EventEmitter();

	parser.onerror = function(err)
	{
		event.emit('error', err.message);
		parser.resume();
	}

	parser.ontext      = function(text) { event.emit('text', text); }
	parser.onopentag   = function(tag)  { event.emit('tag', tag); }
	parser.onattribute = function(attr) { event.emit('attribute', attr); }
	parser.onend       = function()     { event.emit('done'); }

	setTimeout(function()
	{
		try
		{
			readfile(filename).on('read', function(data)
			{
				parser.write(''+data);
			}).on('done', function()
			{
				parser.close();
			}).on('error', function(err)
			{
				event.emit('error', err.message);
			});
		} catch(err) {
			event.emit('error', err.message);
		}
	}, 0);

	return event;
}

//------------------------------------------------------------------------------

module.exports = parsefile;

//------------------------------------------------------------------------------