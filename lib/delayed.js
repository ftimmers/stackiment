
//------------------------------------------------------------------------------

function Delayed()
{
	this.value = undefined;
	this.ready = false;
	this.queue = [];
}

//------------------------------------------------------------------------------

Delayed.prototype.done = function(value)
{
	this.value = value;
	this.ready = true;
	while (this.queue.length)
		this.queue.pop()(value);
}

//------------------------------------------------------------------------------

Delayed.prototype.sync = function(callback)
{
	if (this.ready)
		callback(this.value);
	else
		this.queue.push(callback);
}

//------------------------------------------------------------------------------

Delayed.syncAll = function(pool, callback)
{
	var output = [],
		count = 0;

	function done(i)
	{
		return function(value)
		{
			output[i] = value;
			if (!--count)
				callback(output);
		}
	}

	for (var i = pool.length - 1; i >= 0; --i)
	{
		if (pool[i].constructor == Delayed)
			++count;
		else
			output[i] = pool[i];
	}

	if (!count)
		return callback(output);

	for (var i = pool.length - 1; i >= 0; --i)
		if (pool[i].constructor == Delayed)
			pool[i].sync(done(i));
}

//------------------------------------------------------------------------------

module.exports = Delayed;

//------------------------------------------------------------------------------