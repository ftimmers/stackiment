var fs = require('fs'),
	nl = require('os').EOL,
	events = require('events'),
	spawn = require('child_process').spawn,
	path = __dirname + '/../coreNLP',
	queue = [],
	buffer = '',
	coreNLP;

//------------------------------------------------------------------------------

function initialize()
{
		coreNLP = spawn('java', [
			'-Duser.language=en', // for number seperators
			'-cp', path + '/*',
			'-mx2g',
			'edu.stanford.nlp.sentiment.SentimentPipeline',
			'-output', 'PROBABILITIES',
			'-stdin']);

		/*coreNLP.stderr.on('data', function(line)
		{
			console.log(''+line);
		});*/
		
		coreNLP.stdout.on('data', parse);
}

//------------------------------------------------------------------------------

function terminate()
{
	coreNLP.stdin.end();
}

//------------------------------------------------------------------------------

function sentiment(text)
{
	var event = new events.EventEmitter();
	queue.push(event);

	coreNLP.stdin.write(text
		.replace(/(\r\n|\n)+/g, nl)   // Trim spurious newlines
		.replace(/^(\r\n|\n)+/, '')   // Remove newlines at beginning
		.replace(/\@+/g, '@')         // Trim at-s
		.replace(/(\r\n|\n)?$/, nl + '@@@' + nl)); // Add segment end indicator
	return event;
}

//------------------------------------------------------------------------------

function parse(text)
{
	var index;
	buffer += ''+text;
	
	while ((index = buffer.indexOf('(0 @@@)')) >= 0)
	{
		var data = buffer.substr(0, index);
		buffer = buffer.substr(index + 7);
		
		var results = [],
			event = queue.shift(),
			mean;
		
		data.split('\n').map(function (line)
		{
			line = line.trim();
			if (!line.length) return;
			var match = /0:\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/.exec(line);
			if (match)
			{
				results.push( (+match[1] * 1.0) +
							  (+match[2] * 0.8) +
							  (+match[3] * 0.6) +
							  (+match[4] * 0.4) +
							  (+match[5] * 0.2) );
			}
		});
		
		mean = results.reduce(function(x, y) { return x + y; }) / results.length;
		event.emit('done', mean);
	}
}

//------------------------------------------------------------------------------

if (!fs.existsSync(path))
{
	console.warn('Warning: coreNLP not installed.');
	module.exports = undefined;
}
else
{
	initialize();
	process.on('exit', terminate);
	module.exports = sentiment;
}

//------------------------------------------------------------------------------