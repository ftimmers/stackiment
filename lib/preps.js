var preps = [],
	D = function(x) { return new Date(x); },
	T = function(x) { return ~~(D(x).getTime() / 1000)};

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

preps.add = function(init, process)
{
	process.init = init;
	this.push(process);
}

//------------------------------------------------------------------------------

preps.add([], function ResponseTime(tag)
{
	if (tag.type != 2)
		return;

	if (!(+tag.parentid in this))
		this[+tag.parentid] = T(tag.creationdate);
});

//------------------------------------------------------------------------------

preps.add([], function AverageTime(tag)
{
	if (tag.type != 2)
		return;

	var last = this[+tag.parentid];

	if (!last)
		this[+tag.parentid] = [T(tag.creationdate), 1];
	else
	{
		++last[1];
		last[0] = (last[0] * ((last[1] - 1) / last[1]))
			+ (T(tag.creationdate) / last[1]);
	}
});

//------------------------------------------------------------------------------

preps.add([], function AnswerCommentsCount(tag)
{
	if (tag.type != 2)
		return;

	this[+tag.parentid] = +tag.commentcount + (this[+tag.parentid] || 0);
});

//------------------------------------------------------------------------------

preps.add([], function AcceptedAnswerRating(tag)
{
	if (tag.type == 1)
	{
		if (tag.acceptedanswerid)
			this[+tag.id] = tag.acceptedanswerid;
	}
	else if ((tag.type == 2) && (this[+tag.parentid] === tag.id))
		this[+tag.parentid] = +tag.score;
})

//------------------------------------------------------------------------------

preps.add([0], function Count(tag)
{
	if ((tag.type == 1) && tag.filtered)
		++this[0];
})

//------------------------------------------------------------------------------

module.exports = preps;

//------------------------------------------------------------------------------