var sax = require('sax'),
	Delayed = require('./delayed');

//------------------------------------------------------------------------------

function strip(html)
{
	var parser = sax.parser(false),
		set = {},
		nest = new Nest,
		output = '',
		value = new Delayed,
		done = function()
		{
			value.done(output);
			done = function(){};
		};

	for (var i = arguments.length - 1; i > 0; --i)
		set[arguments[i].toUpperCase()] = true;

	parser.onerror    = done; // We simply ignore the error, possibly impartial
	parser.onopentag  = function(tag)  { nest.push(!(tag.name in set)); }
	parser.onclosetag = function(tag)  { nest.pop(); }
	parser.ontext     = function(text) { if (nest.isOpen()) output += text; }
	parser.onend      = done;

	try { parser.write(html).close(); } catch (e) { done(); }

	return value;
}

//------------------------------------------------------------------------------

function Nest()
{
	this.count = 0;
	this.open = 0;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Nest.prototype.push = function(strip)
{
	if (this.isOpen() && strip)
		++this.open;
	++this.count;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Nest.prototype.pop = function()
{
	if (this.isOpen())
		--this.open;
	--this.count;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Nest.prototype.isOpen = function()
{
	return this.count == this.open;
}

//------------------------------------------------------------------------------

module.exports = strip;

//------------------------------------------------------------------------------
