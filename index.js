#! /usr/bin/env node

var fs = require('fs'),
	nl = require('os').EOL,
	parse = require('./lib/parser'),
	strip = require('./lib/stripper'),
	preps = require('./lib/preps'),
	filters = require('./lib/filters'),
	Delayed = require('./lib/delayed'),
	getopt = require('node-getopt').create([
		['o', 'output=FILE', 'writes output to the specified file'],
		['h', 'help', 'shows this help'],
		['t', 'tag=TAG+', 'posts to include by tag (leave out for all posts)']
	]),
	opt = getopt.bindHelp().parseSystem(),
	options = opt.options,
	input = opt.argv,
	output = console.log,
	tagfilter = /.*/,
	posts = [], // files that contain posts
	data = {}; // used by preprocessors

getopt.setHelp('Usage: stackiment [options] [Posts.xml, ...]\n\n[[OPTIONS]]');

if (!input.length) getopt.showHelp();

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

if (options.output)
{	
	try { fs.truncateSync(options.output, 0); } catch(e) {}
	output = function()
	{
		fs.appendFileSync(options.output,
			Array.prototype.slice.call(arguments).join('\t') + nl);
	}
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

if (options.tag)
	tagfilter = RegExp('<' + options.tag.join('>|<').replace('+', '\\+') + '>');

//------------------------------------------------------------------------------

function preproc(tag)
{
	tag.type = +(tag.posttypeid || 0); // 0-2 rsp. comment, question, answer
	tag.filtered = !!(tag.tags && tagfilter.exec(tag.tags));

	if (tag.type < 1)
		tag.text = strip(tag.text, 'code', 'img');
	else
		tag.body = strip(tag.body, 'code', 'img');

	preps.forEach(function(prep) { prep.call(data[prep.name], tag); });
}

//------------------------------------------------------------------------------

function proc(tag, status)
{
	if (tag.posttypeid != 1) // Post is not a question
		return;

	if (!tagfilter.exec(tag.tags)) // Filter out requested tags
		return;

	++status.count;

	strip(tag.body, 'code', 'img').sync(function (body)
	{
		tag.body = body;

		Delayed.syncAll(filters.map(function (filter)
		{
			return filter.call(data, tag);
		}), function (values)
		{
			output.apply(this, values);
			++data.Done;
			if (options.output) console.info((data.Done / data.Count * 100).toFixed(2) + '%\033[1A');
			--status.count;
		});
	});
}

//------------------------------------------------------------------------------

function prepFile(i)
{
	i = i || 0;
	if (i >= input.length)
	{
		data.Done = 0;
		output.apply(this, filters.map(function(filter) { return filter.header; }));
		processFile();
		return;
	}

	console.log('Pre-processing ' + input[i] + '...');
	parse(input[i]).on('error', function(err)
	{
		console.error('Error:', err);
	}).on('tag', function(tag)
	{
		if (tag.name == 'row')
			preproc(tag.attributes);
		else if (tag.name == 'posts')
			posts.push(input[i]);

	}).on('done', function()
	{
		console.log();
		prepFile(i + 1);
	});
}

//------------------------------------------------------------------------------

function processFile(i)
{
	i = i || 0;
	if (i >= posts.length)
	{
		console.log('Done!');
		process.exit();
		return;
	}

	var status = { count: 0 };

	console.log('Processing ' + posts[i] + '...');
	parse(posts[i]).on('error', function(err)
	{
		console.error('Error:', err);
	}).on('tag', function(tag)
	{
		if (tag.name == 'row')
			proc(tag.attributes, status);
	}).on('done', function done()
	{
		if (!status.count)
		{
			console.log();
			processFile(i + 1);
		}
		else
			setTimeout(done, 1000); // waiting for al tasks to finish
	});
}

//------------------------------------------------------------------------------

(function go()
{
	preps.forEach(function(prep) { data[prep.name] = prep.init; });
	prepFile();
})();

//------------------------------------------------------------------------------