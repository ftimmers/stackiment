var fs = require('fs'),
	es = require('event-stream'),
	nl = require('os').EOL;

if (process.argv.length < 2)
{
	console.info('Usage: node splitter <inputfile.xml> <binsize>');
	process.exit();
}

function proc(line)
{
	input.pause();

	var type = / PostTypeId="(\d+)"/.exec(line),
		id;

	if (!type)
		id = undefined;
	else if (type[1] == 1)
		id = +(/ Id="(\d+)"/.exec(line)[1]);
	else if (type[1] == 2)
		id = +(/ ParentId="(\d+)"/.exec(line)[1]);

	if (id !== undefined)
	{
		id = ~~(id / size);
		if (!(id in files))
		{
			files[id] = outfile(id);
			try { fs.truncateSync(files[id], 0); } catch(e) {}
			fs.appendFileSync(files[id],
				'<?xml version="1.0" encoding="utf-8"?>' + nl +
				'<posts>' + nl +
				line + nl);
		}
		else
			fs.appendFileSync(files[id], line + nl);
	}
	
	input.resume();
}

function done()
{
	for (var i = 0; i < files.length; ++i)
		if (files[i])
			fs.appendFileSync(files[i], '</posts>');
}

var infile = process.argv[2],
	outfile = function(i) { return infile.replace(/(.xml)?$/, i + '.xml'); },
	size = +process.argv[3],
	files = [],
	input = fs.createReadStream(infile)
		.pipe(es.split())
		.pipe(es.mapSync(proc)
			.on('error', function (err) { console.log('Error', err); })
			.on('end', done));

